let name ={
    firstname:"a",
    lastname:"b",
    printfullname:function(k,m){
        console.log(this.firstname+this.lastname+k+m)
    }

}
//three ways of calling  above obj
name.printfullname.call(name,"indiahome","kitchen");//the only difference between apply and  call are the way we pass the arguments .in apply method we pass as list and in acll we pass as normal way
name.printfullname.apply(name,["indiahome","kitchen"]);//here name is refers to this later arguments  will go into  function parameters
name.printfullname();//another way
name.printfullname;//another way

let name2={
    firstname:"h"
}
name.printfullname.call(name2)//function barrowing

let name1 ={
    firstname:"a",
    lastname:"b",
    printfullname:function(home){
        console.log(this.firstname+this.lastname)
    }

}
let name22={
    firstname:"h"
}
name.printfullname.call(name22)//function barrowing

//bind method
let p=name.printfullname.bind(name2,"indiahome","kitchen");//it gives u copy we can invoke it later and it is the only difference between call and bind
console.log(p);
p()