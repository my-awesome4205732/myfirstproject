const posts = [

    { title: 'Post One', body: 'This is post one' },
    
    { title: 'Post Two', body: 'This is post two' } ];
    
    function getPosts() {
        setTimeout(() => {
            let output = '';
            posts.forEach((post) => {
                output += `<li>${post.title}</li>`;
            });
            document.body.innerHTML = `<ul>${output}</ul>`;
        }, 1000);
    }
    function createPost(post) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                posts.push(post);
                const error = false; // Set error to false to indicate success
                if (!error) {
                    resolve();
                } else {
                    reject('Error: Something went wrong');
                }
            }, 2000);
        });
    }
    // createPost({ title: 'Post Three', body: 'This is post three' })
    // .then(getPosts)
    // .catch(err => console.log(err));
const promise1 = Promise.resolve('Hello World');
const promise2 = Promise.resolve(10); // Fixed value for promise2
const promise3 = new Promise((resolve, reject) => setTimeout(resolve, 2000, 'Goodbye')); // Fixed the arrow function syntax
const promise4 = fetch(''); // You should provide a URL here

Promise.all([promise1, promise2, promise3, promise4])
    .then(values => console.log(values))
    .catch(error => console.error(error));